const express = require("express"); // Tương tự : import express from "express";
const path = require("path");

// Khởi tạo Express App
const app = express();

const port = 8000;
// Nơi khai báo API
app.get("/",(req,res)=>{
    console.log(__dirname);

    res.sendFile(path.join(__dirname + "/views/example.html"))
})


app.listen(port, () => {
    console.log(`App Listening on port ${port}`);
})